package com.example.toolis;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.text.DateFormat;
import java.util.Calendar;

public class CatatanForm extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    TextView time, date;
    Switch pengingat;
    Button back,save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catatan_form);
    pengingat=(Switch) findViewById(R.id.pengingat);
    back = findViewById(R.id.back);
    back.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    });
    time=(TextView) findViewById(R.id.jam);
    time.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment timepicker = new TimePicker();
            timepicker.show(getSupportFragmentManager(),"Time Picker");
        }
    });
    date=(TextView) findViewById(R.id.tanggal);
    date.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DialogFragment datepicker = new DatePickerFragment();
            datepicker.show(getSupportFragmentManager(),"Date Picker");
        }
    });
    }
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.YEAR, year);
        String KTgl = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        date.setText(KTgl);

    }
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        time.setText("waktu : " + hourOfDay + ":" + minute);
    }

}
