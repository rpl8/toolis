package com.example.toolis;

import java.sql.Time;
import java.util.Date;

public class data {
    private String judul;
    private Date tgl;
    private Time jam;

public data(String judul,Date tgl,Time jam){
    this.judul = judul;
    this.tgl =tgl;
    this.jam = jam;
}
    public String getJudul() {
        return judul;
    }
    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Time getJam() {
        return jam;
    }

    public void setJam(Time jam) {
        this.jam = jam;
    }

    public Date getTgl() {
        return tgl;
    }
    public void setTgl(Date tgl) {
        this.tgl = tgl;
    }
}
