package com.example.toolis;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final FloatingActionButton mFab       = (FloatingActionButton) findViewById(R.id.fab);
        FloatingActionButton mFabCat    = (FloatingActionButton) findViewById(R.id.fabcatatan);
        FloatingActionButton mFabJad    = (FloatingActionButton) findViewById(R.id.fabjadwal);
        final LinearLayout layCat             = findViewById(R.id.catatanLayout);
        final LinearLayout layJad             = findViewById(R.id.jadwalLayout);
        final Animation mShowButton           = AnimationUtils.loadAnimation(MainActivity.this, R.anim.show_button);
        final Animation mHideButton           = AnimationUtils.loadAnimation(MainActivity.this, R.anim.hide_button);
        final Animation mShowLayout                 = AnimationUtils.loadAnimation(MainActivity.this,R.anim.show_layout);
        final Animation mHideLayout                 = AnimationUtils.loadAnimation(MainActivity.this,R.anim.hide_layout);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layCat.getVisibility() == View.VISIBLE && layJad.getVisibility()== View.VISIBLE){
                    layCat.setVisibility(View.GONE);
                    layJad.setVisibility(View.GONE);
                    layCat.startAnimation(mShowLayout);
                    layJad.startAnimation(mShowLayout);
                    mFab.startAnimation(mHideButton);
                } else{
                    layCat.setVisibility(View.VISIBLE);
                    layJad.setVisibility(View.VISIBLE);
                    layCat.startAnimation(mHideLayout);
                    layJad.startAnimation(mHideLayout);
                    mFab.startAnimation(mShowButton);
                }
            }
        });
        mFabCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent CatForm = new Intent(MainActivity.this, CatatanForm.class);
                startActivity(CatForm);
            }
        });

    }
}
